package br.com.itau;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Pessoa {
    private List<Contato> contatos;
    private String nome;
    private int idade;

    public Pessoa(List<Contato> contatos, String nome, int idade) {
        this.contatos = contatos;
        this.nome = nome;
        this.idade = idade;
    }

    public void addContato(){
        IO io = new IO();

        Map<String, String> dados = io.obterContato();
        Contato contato = new Contato(TipoContato.valueOf(dados.get("tipo")), dados.get("contato"));

        this.contatos.add(contato);
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                ", nome='" + nome + '\'' +
                ", idade=" + idade +
                '}';
    }

}
