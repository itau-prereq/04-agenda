package br.com.itau;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimeMsgInicial() {
        System.out.println("Bem vindo");
    }

    public static void imprimePessoaNotFound() {
        System.out.println("Pessoa nao encontrada");
    }

    public static Map<String, String> obterPessoa(){
        Scanner scanner = new Scanner(System.in);

        Map<String, String> pessoa = new HashMap<>();

        System.out.println("Dados da pessoa ");
        System.out.println("Nome: ");
        String nome = scanner.nextLine();
        System.out.println("Idade: ");
        int idade = scanner.nextInt();

        pessoa.put("nome", nome);
        pessoa.put("idade", String.valueOf(idade));

        return pessoa;
    }

    public static int obterNumeroPessoas() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Numeros de pessoas: ");
        int numero = scanner.nextInt();

        return numero;
    }

    public static String obterNomePessoa() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nome da pessoa: ");
        return scanner.nextLine();
    }

    public static Map<String, String> obterContato() {
        Scanner scanner = new Scanner(System.in);

        Map<String, String> contato = new HashMap<>();

        System.out.println("Tipo do contato: ");
        String tipoContato = scanner.nextLine();
        System.out.println("Contato: ");
        String numero = scanner.nextLine();

        contato.put("tipo", tipoContato);
        contato.put("contato", numero);

        return contato;
    }

    public static void imprimeAgenda(Agenda agenda) {
        for(Pessoa p : agenda.getContatos()){
            System.out.println("Pessoa: " + p.toString());
            System.out.println("contatos: ");
            for(Contato c : p.getContatos()) {
                System.out.println(c.toString());
            }
        }
    }
}
