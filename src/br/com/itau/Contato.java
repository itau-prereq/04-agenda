package br.com.itau;

public class Contato {
    TipoContato tipo;
    String contato;

    public Contato(){

    }

    public Contato(TipoContato tipo, String contato) {
        this.tipo = tipo;
        this.contato = contato;
    }

    @Override
    public String toString() {
        return "{" +
                "tipo=" + tipo +
                ", contato='" + contato + '\'' +
                '}';
    }
}
