package br.com.itau;

public class Email extends Contato {

    public Email(TipoContato tipo, String contato) throws Exception{
        if(!contato.contains("@")){
            throw new Exception("email invalido");
        }
        this.tipo = tipo;
        this.contato = contato;
    }
}
