package br.com.itau;

import java.util.*;

public class Agenda {
    private List<Pessoa> pessoas;

    public Agenda(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public Agenda() {
        this.pessoas = new ArrayList<>();
    }

    public List<Pessoa> getContatos() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public void addPessoa(Pessoa pessoa){
        this.pessoas.add(pessoa);
    }

    public void addPessoa(int nPessoas){
        IO io = new IO();

        for (int i = 0; i < nPessoas; i++) {
            Map<String, String> dados = io.obterPessoa();
            Pessoa pessoa = new Pessoa(new ArrayList<>(), dados.get("nome"), Integer.parseInt(dados.get("idade")));

            addPessoa(pessoa);
        }
    }

    public Pessoa findPessoa(String nomePessoa){
        for(Pessoa p : this.pessoas){
            if (p.getNome() == nomePessoa){
                return p;
            }
        }
        return null;
    }
}
