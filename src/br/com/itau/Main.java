package br.com.itau;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        IO io = new IO();

        Agenda agenda = new Agenda();
        Scanner scanner = new Scanner(System.in);

        String operacao = new String();

        while(operacao != "sair") {
            System.out.println("Digite a opcao desejada: ");
            operacao = scanner.nextLine();

            if (operacao == "adicionar multiplo") {
                agenda.addPessoa(io.obterNumeroPessoas());
                io.imprimeAgenda(agenda);
            } else if (operacao == "adicionar pessoa") {
                agenda.addPessoa(1);
                io.imprimeAgenda(agenda);
            } else if (operacao == "adicionar contato") {
                Pessoa p = agenda.findPessoa(io.obterNomePessoa());
                if(p.equals(null)){
                    io.imprimePessoaNotFound();
                } else {
                    p.addContato();
                }
            } else if (operacao == "imprime"){
                io.imprimeAgenda(agenda);
            } else{
                System.out.println(operacao);
            }
        }
    }
}
